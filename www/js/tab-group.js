$(function(){

    //if multiple tab selector states are defined, set their attributes according to where active is
    var setTabs = function (group){
        if($(".data-tab-button", group).attr("data-tab-state"))
        {
            var active = $(".data-tab-content.active", group).attr("id");
            $(".data-tab-button", group).each(function(){
                if (($(this).attr("data-tab-state") == "on" && $(this).attr("data-tab-for") == active) ||
                    ($(this).attr("data-tab-state") == "off" && $(this).attr("data-tab-for") != active))
                {
                    $(this).addClass("active");
                    $(this).removeClass("hidden");
                }
                else
                {
                    $(this).addClass("hidden");
                    $(this).removeClass("active");
                };
            });
        }
    }

    var activateTabs = function (){
        $(".tab-group").each(function(){
            //if no active is given as default, make first tab active
            var group = $( ".data-tab-content", $(this));
            if (group.hasClass("active"))
            {
                //do nothing
            }
            else
            {
                group.first().addClass("active");
            };

            setTabs($(this));
        });
    }

    $( ".data-tab-button" ).click(function() {
        var group = $(this).closest(".tab-group");
        $( ".data-tab-content", group).removeClass("active");
        $( "#" + $(this).attr("data-tab-for")).addClass("active");
        setTabs(group);
        activateTabs();
    });

    activateTabs();
});
