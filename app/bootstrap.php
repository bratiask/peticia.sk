<?php

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

//$configurator->setDebugMode(TRUE);  // debug mode MUST NOT be enabled on production server
$configurator->enableDebugger(__DIR__ . '/../log');

$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->addDirectory(__DIR__ . '/../vendor/others')
	->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon');

$container = $configurator->createContainer();

$db = $container->parameters['database'];

$container->application->onStartup[] = function() use ($db) {
    ActiveRecord\Config::initialize(function($cfg) use ($db)
    {
        ActiveRecord\Connection::$datetime_format = 'Y-m-d H:i:s';
        ActiveRecord\DateTime::$DEFAULT_FORMAT = 'Y-m-d H:i:s';

//        class ActiveRecordLogger {
//            public function log($message)
//            {
//                throw new Exception($message);
//            }
//        }
//
//        $cfg->set_logging(true);
//        $cfg->set_logger(new ActiveRecordLogger);
//
        $cfg->set_model_directory(__DIR__ . '/model');
        $cfg->set_connections(array(
            'development' => $db['driver'] . '://' . $db['user'] . ':' . $db['password'] . '@' . $db['host'] . '/' . $db['db_name'] . '?charset=utf8'));
    });
};

return $container;
