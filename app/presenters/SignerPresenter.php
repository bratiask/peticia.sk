<?php

namespace App\Presenters;

use Nette,
    \User,
    \Petition,
    \Signer,
    \PHPExcel,
    \PHPExcel_Settings,
	App\Model;

class SignerPresenter extends BasePresenter
{

	public function renderDefault($id)
	{
        if (!$id)
        {
            $this->redirect('Homepage:default');
        }
        else
        {
            $petition = Petition::find($id);
            $this->template->petition = $petition;
            $this->template->signers = $petition->signers ? $petition->signers : null;
        }
	}

    public function actionDelete($signer_id, $petition_id)
    {
        $signer = Signer::find($signer_id);
        $signer->delete();

        $this->redirect('Signer:default', $petition_id);
    }
}
