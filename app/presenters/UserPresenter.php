<?php

namespace App\Presenters;

use Nette,
    \User,
	App\Model;


class UserPresenter extends BasePresenter
{

	public function renderDefault()
	{
        $this->template->users = User::all();
	}

    public function renderEdit($id = 0)
    {
        if ($id != 0)
        {
            $user = User::find($id);
            $this['editForm']->setDefaults($user->to_array());
        }
    }

    protected function createComponentEditForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addText('name', 'Meno')
            ->setRequired('Prosím vyplňte svoje meno.');

        $form->addHidden('id');

        $form->addText('surname', 'Priezvisko')
            ->setRequired('Prosím vyplňte svoje priezvisko.');

        $form->addText('email', 'E-mail')
            ->setRequired('Prosím vyplňte svoj e-mail.');

        $form->addPassword('password', 'Heslo')
            ->setRequired('Prosím vyplňte svoje heslo.');

        $form->addSubmit('submit', 'Uložiť')
            ->onClick[]= array($this, 'editFormSubmitted');
        $form->addSubmit('cancel', 'Zrušiť')
            ->setValidationScope(FALSE)
            ->onClick[]= array($this, 'editFormCancelled');

        return $form;
    }

    public function editFormCancelled()
    {
        $this->redirect('Homepage:default');
    }

    public function editFormSubmitted(Nette\Forms\Controls\SubmitButton $btn)
    {
        $values = $btn->form->getValues();

        if($values->id)
        {
            $user = User::find($values->id);
        }
        else
        {
            $user = new User();
        }

        $user->name = $values->name;
        $user->surname = $values->surname;
        $user->email = $values->email;
        $user->password = md5($values->password);

        $user->save();

        $this->redirect('Homepage:default');
    }

    public function actionDelete($id)
    {
        $user = User::find($id);
        $user->delete();

        $this->redirect('Homepage:default');
    }
}
