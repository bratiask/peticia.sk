<?php

namespace App\Presenters;

use Nette,
    \User,
    \Petition,
    \Signer,
    \PHPExcel,
    \PHPExcel_Settings,
    \PHPExcel_IOFactory,
	App\Model;

class PetitionPresenter extends BasePresenter
{

	public function renderDefault($id)
	{
        if (!$id)
        {
            $this->redirect('Homepage:default');
        }
        else
        {
            $petition = Petition::find($id);
            $this->template->petition = $petition;
            $this->template->signers = $petition->signers ? $petition->signers : null;
            $this['signerForm']->setDefaults(array('petition_id' => $petition->id));
        }
	}

    public function renderManage()
    {
        $this->template->petitions = Petition::all();
    }

    public function renderEdit($id = 0)
    {
        if ($id != 0)
        {
            $petition = Petition::find($id);
            $this['editForm']->setDefaults($petition->to_array());
        }
    }

    protected function createComponentSignerForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addHidden('petition_id');

        $form->addText('first_name', 'Meno')
            ->setRequired('Prosím vyplňte svoje meno.');

        $form->addText('last_name', 'Priezvisko')
            ->setRequired('Prosím vyplňte svoje priezvisko.');

        $form->addText('address', 'Adresa')
            ->setRequired('Prosím vyplňte svoju adresu.');

        $form->addText('psc', 'PSČ')
            ->setRequired('Prosím vyplňte svoje PSČ.');

        $form->addText('city', 'Mesto')
            ->setRequired('Prosím vyplňte svoje mesto mesto.');;

        $form->addText('occupation', 'Zamestnanie');

        $form->addText('email', 'E-mail');

        $publish = array(
            'public' => 'zobraziť všetko',
            'protected' => 'skryť adresu',
            'private' => 'skryť všetko'
        );

        $form->addRadioList('display_type', 'Zobrazenie podpisu:', $publish);

        $form->addSubmit('submit', 'Podpísať')
            ->onClick[]= array($this, 'signerFormSubmitted');

        return $form;
    }

    protected function createComponentEditForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addText('title', 'Názov')
            ->setRequired('Prosím vyplňte názov petície.');

        $form->addHidden('id');

        $form->addText('subtitle', 'Podnadpis');

        $form->addText('url_slug', 'URL')
            ->setRequired('Prosím vyplňte URL.');

        $form->addTextArea('organizer_name', 'Organizuje')
            ->setRequired('Prosím vyplňte organizátrora.');

        $form->addTextArea('perex', 'Krátky popis');

        $form->addTextArea('description', 'Text petície')
            ->setRequired('Prosím vyplňte text petície.');

        $form->addCheckbox('is_active', 'Aktivovať');

        $form->addCheckbox('is_published', 'Publikovať');

        $form->addSubmit('submit', 'Uložiť')
            ->onClick[]= array($this, 'editFormSubmitted');
        $form->addSubmit('cancel', 'Zrušiť')
            ->setValidationScope(FALSE)
            ->onClick[]= array($this, 'editFormCancelled');

        return $form;
    }

    public function editFormCancelled()
    {
        $this->redirect('Homepage:default');
    }

    public function editFormSubmitted(Nette\Forms\Controls\SubmitButton $btn)
    {
        $values = $btn->form->getValues();

        if ($values->id){
            $petition = Petition::find($values->id);
        }
        else
        {
            $petition = new Petition();
        }

        $petition->title = $values->title;
        $petition->subtitle = $values->subtitle;
        $petition->url_slug = $values->url_slug;
        $petition->organizer_name = $values->organizer_name;
        $petition->perex = $values->perex;
        $petition->description = $values->description;
        $petition->is_active = $values->is_active;
        $petition->is_published = $values->is_published;

        $petition->save();

        $this->redirect('Homepage:default');
    }

    public function signerFormSubmitted(Nette\Forms\Controls\SubmitButton $btn)
    {
        $values = $btn->form->getValues();

        $signer = new Signer();

        $signer->petition_id = $values->petition_id;
        $signer->first_name = $values->first_name;
        $signer->last_name = $values->last_name;
        $signer->address = $values->address;
        $signer->psc = $values->psc;
        $signer->city = $values->city;
        $signer->occupation = $values->occupation;
        $signer->email = $values->email;
        $signer->display_type = $values->display_type;

        $signer->save();

        $this->redirect('Petition:signed');
    }

    public function actionDelete($id)
    {
        $petition = Petition::find($id);
        $petition->delete();

        $this->redirect('Homepage:default');
    }

    public function actionExport($petition_id)
    {
        $objPHPExcel = new PHPExcel();

        $petition = Petition::find($petition_id);

        $signers = $petition->signers;

        PHPExcel_Settings::setLocale('sk');

        $sheet = $objPHPExcel->getSheet(0);

        $sheet->setCellValue('A1', 'Meno');
        $sheet->setCellValue('B1', 'Priezvisko');
        $sheet->setCellValue('C1', 'Adresa');
        $sheet->setCellValue('D1', 'PSČ');
        $sheet->setCellValue('E1', 'Mesto');
        $sheet->setCellValue('F1', 'Zamestnanie');
        $sheet->setCellValue('G1', 'E-mail');

        $row = 1;

        foreach ($signers as $signer)
        {
            $row++;
            $sheet->setCellValue('A'.$row, $signer->first_name);
            $sheet->setCellValue('B'.$row, $signer->last_name);
            $sheet->setCellValue('C'.$row, $signer->address);
            $sheet->setCellValue('D'.$row, $signer->psc);
            $sheet->setCellValue('E'.$row, $signer->city);
            $sheet->setCellValue('F'.$row, $signer->occupation);
            $sheet->setCellValue('G'.$row, $signer->email);
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        header('Content-type: application/vnd.ms-excel');

        header('Content-Disposition: attachment; filename="podpisy-'.$petition->url_slug.'-'.StrFTime("%d-%m-%YS", Time()).'.xls"');

        $objWriter->save('php://output');

        $this->terminate();
    }
}
