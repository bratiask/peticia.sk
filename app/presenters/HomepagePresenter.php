<?php

namespace App\Presenters;

use Nette,
    \User,
    \Petition,
	App\Model;


/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{
	public function renderDefault()
	{
        if($this->user->isLoggedIn())
        {
            $this->template->users = User::all();
		    $this->template->petitions = Petition::all();
        }
        else
        {
            $this->template->petitions = Petition::find_all_by_is_published(TRUE);
        }
	}

    public function renderRfe()
    {

    }
}
