<?php
use Nette\Security as NS;

class Authenticator extends Nette\Object implements NS\IAuthenticator
{
    function authenticate(array $credentials)
    {
        list($email, $password) = $credentials;
        $row = User::find_by_email($email);

        if (!$row) {
            throw new NS\AuthenticationException('User not found.');
        }

        if ($row->password !== md5($password)) {
            throw new NS\AuthenticationException('Invalid password.');
        }

        return new NS\Identity($row->id);
    }
}