<?php

class CreateUsersTable extends Ruckusing_Migration_Base
{
    public function up()
    {
        $t = $this->create_table('users', array('options' => 'Engine=InnoDB','auto_increment' => true));

        $t->column('name', 'string');
        $t->column('surname', 'string');
        $t->column('email', 'string');
        $t->column('password', 'string', array('limit' => 40));

        $t->finish();
    }//up()

    public function down()
    {
        $this->drop_table('users');
    }//down()
}
