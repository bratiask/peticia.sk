<?php

class CreateSignersTable extends Ruckusing_Migration_Base
{
    public function up()
    {
        $t = $this->create_table('signers', array('options' => 'Engine=InnoDB','auto_increment' => true));

        $t->column('petition_id', 'integer');
        $t->column('first_name', 'string');
        $t->column('last_name', 'string');
        $t->column('address', 'string');
        $t->column('psc', 'string');
        $t->column('city', 'string');
        $t->column('occupation', 'string');
        $t->column('email', 'string');
        $t->column("display_type", "string");

        $t->finish();
    }//up()

    public function down()
    {
        $this->drop_table('signers');
    }//down()
}
