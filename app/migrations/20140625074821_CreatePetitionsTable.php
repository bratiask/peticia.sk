<?php

class CreatePetitionsTable extends Ruckusing_Migration_Base
{
    public function up()
    {
        $t = $this->create_table('petitions', array('options' => 'Engine=InnoDB','auto_increment' => true));

        $t->column('title', 'string');
        $t->column('subtitle', 'string');
        $t->column('url_slug', 'string');
        $t->column('organizer_name', 'text');
        $t->column('perex', 'text');
        $t->column('description', 'text');
        $t->column('is_active', 'boolean');
        $t->column('is_published', 'boolean');

        $t->finish();
    }//up()

    public function down()
    {
        $this->drop_table('petitions');
    }//down()
}
