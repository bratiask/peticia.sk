<?php

return array(
    'db' => array(
        'development' => array(
            'type'      => 'mysql',
            'host'      => '127.0.0.1',
            'port'      => 3306,
            'database'  => 'rfemanagement',
            'user'      => 'root',
            'password'  => '',
            'directory' => '',
            'charset' => 'utf8'
        ),
    ),
    'migrations_dir' => RUCKUSING_WORKING_BASE . '/app/migrations',
    'db_dir' => RUCKUSING_WORKING_BASE . '/app/db',
    'log_dir' => RUCKUSING_WORKING_BASE . '/log'
);

